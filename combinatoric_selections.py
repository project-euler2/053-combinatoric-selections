import math
import time
start_time = time.time()

def num_combinatoric(min,n_max):
    total = 0
    for n in range (0,n_max+1):
        for r in range(0,n-1):
            if ((math.factorial(n))/((math.factorial(r))*(math.factorial(n-r)))) >= min:
                total+=1
    return total

print(num_combinatoric(1000000,100))
print(f"--- {(time.time() - start_time):.10f} seconds ---" )